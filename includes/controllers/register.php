<?php

if(!isset($_SESSION['register_form'])) {
  $register_form = hash('md5',time().'Budget'.rand(100,999).'Book'.'Registration');
  $_SESSION['register_form'] = $register_form;
} else {
  $register_form = $_SESSION['register_form'];
}

$register_try = false;
$registration = false;

if(isset($_POST['user']) and strlen($_POST['user']) > 4
   AND isset($_POST[$register_form]) and strlen($_POST[$register_form]) > 6 AND
   isset($_POST['repetition']) AND $_POST[$register_form] === $_POST['repetition']) {

  $register = new Registration($this->config['mysql']['prefix'], $this->sql);
  $registration = $register->register($_POST['user'],$_POST[$register_form]);

}

echo $this->twig->render('register.html.twig', [
    'password_field_name' => $register_form,
    'register_try'        => $register_try,
    'registration'        => $register_try,
  ]);

?>
