<?php

if(!isset($_SESSION['login_form'])) {

  if( $this->config['general']['random_login_fields'] ) {
    $login_form = hash('md5',time().'Budget'.rand(100,999).'Book');
  } else {
    $login_form = 'password';
  }

  $_SESSION['login_form'] = $login_form;
} else {
  $login_form = $_SESSION['login_form'];
}

$login_try = false;
$login_result = false;

# Login try #
if( ( isset($_POST['user']) AND strlen($_POST['user']) > 4) AND
    ( isset($_POST[$login_form]) AND strlen($_POST[$login_form]) > 6) ) {

  $login_try = true;
  $authenticate = new Authentication($this->config['mysql']['prefix'], $this->sql);
  $login_result = $authenticate->login(sprintf('%s',$_POST['user']),sprintf('%s',$_POST[$login_form]));

}

echo $this->twig->render('login.html.twig', [
    'password_field_name' => $login_form,
    'login_try'           => $login_try,
    'login_success'       => $login_result,
  ]);

?>
