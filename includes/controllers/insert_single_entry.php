<?php

$insert_attempt = false;
$insert = false;

if(isset($_POST) and count($_POST) > 1) {
  $insert_attempt = true;

  $insert = $this->operator->insert_entry(
    $_POST['date'],
    $_POST['outcome'],
    $_POST['income'],
    $_POST['location'],
    $_POST['city'],
    $_POST['comment'],
    $_POST['group']
  );

}

echo $this->main->twig->render('insert_single.html.twig', [
  'date' => date('d.n.Y',time()),
  'insert_attempt' => $insert_attempt,
  'insert_success' => $insert
  ]);

?>
