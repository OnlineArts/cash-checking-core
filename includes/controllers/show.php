<?php

$has_entries = false;
$results = array();

if(isset($_GET['limit'])) {
  if($_GET['limit'] == '0') {
    $limit = 100000;
  } else {
    $limit = intval(sprintf('%d',$_GET['limit']));
  }
} else $limit = 20;

$statement_query = "
  SELECT
    id,
    date,
    AES_DECRYPT(dc_outcome, ?) outcome,
    AES_DECRYPT(dc_income, ?) income,
    AES_DECRYPT(dc_location, ?) location,
    AES_DECRYPT(dc_city, ?) city,
    AES_DECRYPT(dc_comment, ?) `comment`,
    AES_DECRYPT(dc_group, ?) `group`
  FROM
    `".$this->config['mysql']['prefix']."view_entry`
  WHERE
    user_id = ?
  ORDER BY
    date DESC,
    id DESC
  LIMIT ?;";

if( $statement = $this->sql->prepare($statement_query) ) {

  $result = array(
    'id' => NULL,
    'date'=> NULL,
    'outcome'=> NULL,
    'income'=> NULL,
    'location'=> NULL,
    'city'=> NULL,
    'comment'=> NULL,
    'group'=> NULL
  );

  $statement->bind_param('ssssssii',
    $this->key,
    $this->key,
    $this->key,
    $this->key,
    $this->key,
    $this->key,
    $this->user,
    $limit
  );

  $statement->execute();

  $statement->bind_result(
    $result['id'],
    $result['date'],
    $result['outcome'],
    $result['income'],
    $result['location'],
    $result['city'],
    $result['comment'],
    $result['group']
  );

  $has_entries = $statement === false;
  while($statement->fetch()) {
    $results[] = array(
      'id'       => $result['id'],
      'date'     => date('d.m.Y',$result['date']),
      'outcome'  => $result['outcome'],
      'income'   => $result['income'],
      'location' => $result['location'],
      'city'     => $result['city'],
      'comment' => $result['comment'],
      'group'   => $result['group']
    );
  }
  $statement->close();

}

echo $this->main->twig->render('show.html.twig', [
  'has_entries' => $has_entries,
  'show_id'     => (isset($_GET['id'])),
  'entries'     => $results,
  ]);

?>
