<?php

class BudgetBook {

  private $config;
  private $sql;
  private $key;

  private $operator;

  private $user = 1;

  function __construct(&$config, &$sql, &$key, &$user_id, &$main) {

    $this->config = $config;
    $this->sql = $sql;
    $this->key = $key;
    $this->user = $user_id;
    $this->main = $main;

    $this->operator = new Operator(
      $this->config['mysql']['prefix'],
      $this->sql,
      $this->key,
      $this->user
    );

    if(isset($_GET['insert'])) {
      $this->insert();
    } elseif(isset($_GET['show'])) {
      $this->show();
    } elseif(isset($_GET['list'])) {
      $this->operator->list($_GET['list']);
    } elseif(isset($_GET['parser'])) {
      $this->parser($_GET['parser']);
    } else $this->show();

  }

  private function show() {
    require_once('includes/controllers/show.php');
  }

  private function insert() {
    require_once('includes/controllers/insert_single_entry.php');
  }

}

?>
