<?php

/**
 * Allows registration process
 *
 * The registration uses the Authentication class for generate the aes key
 * and forward to a login after a successfull register
 *
 * @author  Online Arts, Roman Martin <r.martin@online-arts.de>
 */
class Registration extends Authentication {

  private $table_prefix;
  private $sql;

  public function __construct(string $table_prefix, $sql) {
    parent::__construct($table_prefix, $sql);
    $this->sql = $sql;
    $this->table_prefix = $table_prefix;
  }

  /**
   * Starts the register process
   *
   * The function starts with the registration process. The  given user name
   * will be searched in the database. An unknown user credential will be
   * stored.
   *
   * Optionally, a login will be triggered.
   *
   * @param string $username  the raw user name
   * @param string $password  the raw user password
   * @return bool  true if registration was successfull
   */
  public function register(string &$username, string &$password, bool $proceed_to_login = true) : bool {
    $user_id = $this->get_user_id($username);
    if($user_id) return false;

    $password_hash = password_hash($password, PASSWORD_BCRYPT, array('cost' => 16));
    $new_user_id = $this->insert_new_user($username, $password);
    if(!$new_user_id) return false;

    if( $proceed_to_login ) {
      $aes_key = $this->generate_aes_key($password);
      $this->post_login($aes_key, $new_user_id, false);
    }

    return true;
  }

  /**
   * Inserts a new user entity into the database
   *
   * @param string $username  the raw user name
   * @param string $password_hash  the hashed user password
   * @return int   return the new user id or a 0 for a failed insertion.
   */
  public function insert_new_user(string $username, string $password_hash) : int {
    $timestamp = time();

    $statement_query = "
      INSERT INTO
        ".$this->table_prefix."user
        (user_name, user_password, user_active, user_register_time)
      VALUES
        (?, ?, 1, ? );"; # TODO: Remove user_active for production

    if($statement = $this->sql->prepare($statement_query)) {
      $statement->bind_param('ssi',$username,$password_hash, $timestamp);
      $statement->execute();
      $last_id = $statement->insert_id;
      if($last_id != NULL and is_numeric($last_id)) return $last_id;
    }

    return 0;
  }

}

?>
