<?php

/**
 * Provides basic methods for user authentication
 *
 * This class provides the basic steps for user authentication and generates
 * the final necessary AES key.
 *
 * @author  Online Arts, Roman Martin <r.martin@online-arts.de>
 */
class Authentication {

  private $table_prefix;
  private $sql;

  public function __construct(string $table_prefix, $sql) {
    $this->table_prefix = $table_prefix;
    $this->sql = $sql;
  }

  /**
   * Starts the login process
   *
   * The function starts the login process by searching for a corresponding
   * user name or user email address. Subsequently, the password hash will be
   * computed and compared. A matching password will be finished by the AES
   * key generation and a successful login.
   *
   * An unknown user name or user email will lead to fake password hash
   * computation, pseudo key generation, and a failed login.
   *
   * @param string $username  the raw user name or user email
   * @param string $password  the raw user password
   * @return bool  true if login was successfull
   */
  public function login(string $user, string $password) : bool {
    $method = (filter_var($user, FILTER_VALIDATE_EMAIL)) ? true : false;

    $user_id = $this->get_user_id($user,$method);
    if(!$user_id) return $this->emulate_login();

    $password_hash = $this->get_user_password_hash($user_id);
    if(password_verify($password, $password_hash) !== TRUE) return false;

    $aes_key = $this->get_aes_key($password, $user_id);

    $this->post_login($aes_key, $user_id);

    return true;
  }

  /**
   * Searches for a user id by the user name or user email.
   *
   * @param string $username  the raw user name or user email
   * @param bool   $email_as_user  true if user email was passed
   * @return int   return the corresponding user id or a 0
   */
  protected function get_user_id(string &$user, bool &$email_as_user = false) : int {
    $id = 0;

    if($email_as_user) {
      $statement_query = "SELECT user_id FROM ".$this->table_prefix."user WHERE user_email=? LIMIT 1";
    } else {
      $statement_query = "SELECT user_id FROM ".$this->table_prefix."user WHERE user_name=? LIMIT 1";
    }

    if($statement = $this->sql->prepare($statement_query)) {
      $statement->bind_param('s',$user);
      $statement->execute();
      $statement->bind_result($result);
      $statement->fetch();
      if($result != NULL) $id = intval($result);
      $statement->close();
    }

    return $id;
  }

  /**
   * Generates a random password hash and a pseudo AES key.
   *
   * @return false will lead to an aborted login process
   */
  private function emulate_login() : bool {
    $fake_password = hash('md5',time().rand(100,999));
    password_verify($fake_password, '$2y$16$.zz2vhHJf0ObfQ8CD/gjK.5cvjoAAFE7VOM1XuZ0zHIyABayDAQo.');
    $this->generate_aes_key($fake_password);

    return false;
  }

  /**
   * Retrieve a user password hash for a given user id.
   *
   * @param int     $user_id  valid user id
   * @return string stored password hash or an empty string
   */
  private function get_user_password_hash(int &$user_id) : string {
    $statement_query = "SELECT user_password FROM ".$this->table_prefix."user WHERE user_id=? LIMIT 1";
    if($statement = $this->sql->prepare($statement_query)) {
      $statement->bind_param('i',$user_id);
      $statement->execute();
      $statement->bind_result($result);
      $statement->fetch();
      if($result != NULL) {
        $statement->close();
        return sprintf('%s',$result);
      }
    }

    return '';
  }

  /**
   * Finalizes the login process by storing the user id and AES key to a
   * current session.
   *
   * @param int     $user_id  valid user id
   * @param bool    $update_login  if true the current timestamp will be stored
   * @return string stored password hash
   */
  protected function post_login(string &$aes_key, int &$user_id, bool $update_login = true) : void {

    if($update_login) $this->update_last_login($user_id);

    $_SESSION['aes'] = $aes_key;
    $_SESSION['user'] = $user_id;
  }

  /**
   * Saves the current timestamp to corresponding user table entry
   *
   * @param int   $user_id  valid user id
   * @return bool true if a successful update was possible
   */
  private function update_last_login(int &$user_id) : bool {
    $statement_query = "UPDATE ".$this->table_prefix."user SET user_last_login = ? WHERE user_id = ? ;";

    if( $statement = $this->sql->prepare($statement_query) ) {
      $timestamp = time();
      $statement->bind_param('ii', $timestamp, $user_id);
      $result = $statement->execute();

      return (!($result === false));
    }

    return false;
  }

  # TODO: Remove; necessary for back-compatibility get_aes_key -> generate_aes_key
  private function get_aes_key(string &$password, int &$user_id) : string {
    $aes_key = $this->generate_aes_key($password);
    $get_override_key = $this->get_override_key($user_id);

    return ($get_override_key != NULL AND strlen($get_override_key)) ? $get_override_key : $aes_key;
  }

  /**
   * Generates the final AES key.
   *
   * Multiple nested iterations of SHA256 hashing finaly generates the user AES
   * key by the initial vector from the user password hash.
   *
   * @param string  $password_hash  the password hash from a user
   * @return string the final AES key
   */
  protected function generate_aes_key(string &$password_hash) : string {
    $aes_key = '';
    $aes = $password_hash;
    for($i = 0; $i < 999999; $i++) {
    	$aes = hash('sha256',$aes);
    }
    $aes_key = $aes;

    return $aes_key;
  }

  # TODO: Remove; necessary for back-compatibility
  private function get_override_key(int &$user_id) {
    $statement_query = "SELECT user_key_override FROM ".$this->table_prefix."user WHERE user_id=? LIMIT 1";
    if($statement = $this->sql->prepare($statement_query)) {
      $statement->bind_param('i',$user_id);
      $statement->execute();
      $statement->bind_result($result);
      $statement->fetch();
      $statement->close();
      return $result;
    }
  }

}

?>
