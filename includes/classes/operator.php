<?php

class Operator {

  private $sql;
  private $table_prefix;

  public function __construct(string $table_prefix, $sql, $key, $user_id) {
    $this->table_prefix = $table_prefix;
    $this->sql = $sql;
    $this->key = $key;
    $this->user = $user_id;
  }

  # TODO: Adapt since it's working with by MySQL
  private function map_entry_table(string $target) : array {
    $identifier = array();

    switch($target) {
      case 'location':
        $identifier = array(
          'table_name'  => 'location',
          'table_uid'   => 'location_id',
          'table_value' => 'location_name',
        );
      break;
      case 'city':
        $identifier = array(
          'table_name'  => 'city',
          'table_uid'   => 'city_id',
          'table_value' => 'city_name',
        );
      break;
      case 'comment':
        $identifier = array(
          'table_name'  => 'comment',
          'table_uid'   => 'comment_id',
          'table_value' => 'comment_name',
        );
      break;
      case 'group':
        $identifier = array(
          'table_name'  => 'group',
          'table_uid'   => 'group_id',
          'table_value' => 'group_name',
        );
      break;
    }

    return $identifier;
  }

  private function get_id_from_entry_table(string $value, array $group) : int {

    $exist_id = $this->check_id_from_entry_table($value, $group);
    if($exist_id) return $exist_id;

    $new_id = $this->insert_value_for_entry_table($value, $group);
    return $new_id;
  }

  private function check_id_from_entry_table(string &$value, array &$group) : int {
    $return = 0;

    $statement_query = "
      SELECT
        ".$group['table_uid']."
      FROM
        `".$this->table_prefix.$group['table_name']."`
      WHERE
        user_id = ? AND
        ".$group['table_value']." = AES_ENCRYPT(?, ?)
      LIMIT 1";

    if($statement = $this->sql->prepare($statement_query)) {
      $statement->bind_param('iss', $this->user, $value, $this->key);
      $statement->execute();
      $statement->bind_result($result);
      $statement->fetch();
      $statement->close();
      if($result != NULL) return intval($result);
    }

    return $return;
  }

  private function insert_value_for_entry_table(string &$value, array &$group) : int {
    $statement_query = "
    INSERT INTO
      `".$this->table_prefix.$group['table_name']."`
    (".$group['table_value'].", user_id)
    VALUES ( AES_ENCRYPT(?, ?), ? );";

    if($statement = $this->sql->prepare($statement_query)) {
      $statement->bind_param('ssi', $value, $this->key, $this->user);
      $statement->execute();
      $insert_id = $statement->insert_id;
      $statement->close();
      if($insert_id != NULL and is_numeric($insert_id)) return $insert_id;
    }

    return 0;
  }

  private function check_insert_value(&$value, string $group_name) {
    if($value != NULL AND strlen($value)) {
      $group_array = $this->map_entry_table($group_name);

      if(count($group_array)) {
        $id = $this->get_id_from_entry_table($value, $group_array);

        if($id) return $id;
      }
    }

    return null;
  }

  public function insert_entry($date, $outcome = Null, $income = Null, $location = Null, $city = Null, $comment = Null, $group = Null) {
    $dt = new DateTime($date);
    $timestamp = $dt->getTimestamp();

    if(($income == NULL and strlen($income)) and ($outcome == NULL and strlen($outcome))) {
       echo 'Nothing to insert';
       return false;
     }

    $outcome = ($outcome != NULL AND strlen($outcome)) ? sprintf("%.2f",floatval(str_replace(',','.',$outcome))) : NULL;
    $income = ($income != NULL AND strlen($income)) ? sprintf("%.2f",floatval(str_replace(',','.',$income))) : NULL;
    $location = $this->check_insert_value($location, 'location');
    $city = $this->check_insert_value($city, 'city');
    $comment = $this->check_insert_value($comment, 'comment');
    $group = $this->check_insert_value($group, 'group');

    $statement_query = "
      INSERT INTO
        ".$this->table_prefix."entry
        (entry_date, entry_outcome, entry_income, entry_location, entry_city, entry_comment, entry_group, user_id)
      VALUES
        ( ?, AES_ENCRYPT(?, ?), AES_ENCRYPT(?, ?), ?, ?, ?, ?, ? );";

    if($statement = $this->sql->prepare($statement_query)) {

      $statement->bind_param('issssiiiii',
        $timestamp, $outcome, $this->key, $income, $this->key,
        $location, $city, $comment, $group, $this->user
      );

      $statement->execute();

      $insert_id = $statement->insert_id;
      if($insert_id != NULL and is_numeric($insert_id)) {
        return $insert_id;
      } else {
        echo $statement->error;
      }

    }

    return false;
  }

  public function list(string &$list) {
    header('Content-Type: application/json');

    $group = $this->map_entry_table($list);
    if(!count($group)) die('[ ]');

    $statement_query = "
    SELECT
      AES_DECRYPT(dc_".$group['table_name'].", ?) r,
      count(dc_".$group['table_name'].") as frequency,
      user_id
    FROM
      `".$this->table_prefix."view_entry`
    WHERE
      user_id = ? AND
      dc_".$group['table_name']." IS NOT NULL
    GROUP BY
      dc_".$group['table_name'].", user_id
    ORDER BY
      frequency DESC
    LIMIT 100";

    $collector = array();
    $result;
    $frequency;
    $user_id;

    if( $statement = $this->sql->prepare($statement_query) ) {
      $statement->bind_param('si', $this->key, $this->user);
      $statement->execute();
      $statement->bind_result($result,$frequency,$user_id);

      while($statement->fetch()) {
        if($result != NULL AND strlen($result))
          $collector[] = $result;
      }
      $statement->close();
      echo json_encode($collector);
      return true;
    }

    return false;
  }

}

?>
