<?php

class Main {

  private $config;
  private $sql;
  public $twig;

  function __construct(&$config, &$twig) {

    if($config != NULL AND count($config['mysql']) >= 4) {
      $this->config = $config;
      unset($config);

      $this->sql = new mysqli(
        $this->config['mysql']['host'],
        $this->config['mysql']['user'],
        $this->config['mysql']['password'],
        $this->config['mysql']['database']
      );

    } else exit('No config data');
    $this->twig = $twig;

    /* Check login state */
    if(isset($_SESSION['aes']) and isset($_SESSION['user'])) {
      $app = new BudgetBook($this->config, $this->sql, $_SESSION['aes'], $_SESSION['user'], $this);
    } elseif(isset($_GET['register'])) {
      $this->register_form();
    } else {
      $this->login_form();
    }

  }

  private function login_form() {
    require_once('includes/controllers/login.php');
  }

  private function register_form() {
    require_once('includes/controllers/register.php');
  }

  function __destruct() {
    $this->sql->close();
  }

}

?>
