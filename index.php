<?php

# if possible save outside web directory
$config_path = 'config/config.ini';

session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$config = parse_ini_file($config_path, true);
require_once('includes/main.php');
require_once('includes/application.php');
require_once('includes/classes/authentication.php');
require_once('includes/classes/registration.php');
require_once('includes/classes/operator.php');

# include twig template engine
require_once('vendor/autoload.php');
$loader = new \Twig\Loader\FilesystemLoader(
  'templates/'.$config['general']['template']);
$twig = new \Twig\Environment($loader, array(
    'strict_variables' => true,
    'debug' => true,
    'cache'=> false
));

$application = new Main($config, $twig);

?>
