$( function() {
  $.datepicker.regional['de'] = {
  closeText: 'Done',
  prevText: 'Prev',
  nextText: 'Next',
  currentText: 'heute',
  monthNames: ['Januar','Februar','März','April','Mai','Juni',
  'Juli','August','September','Oktober','November','Dezember'],
  monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun',
  'Jul','Aug','Sep','Okt','Nov','Dez'],
  dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
  dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
  dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
  weekHeader: 'KW',
  dateFormat: 'dd.mm.yy',
  firstDay: 0,
  isRTL: false,
  showMonthAfterYear: false,
  yearSuffix: ''};

  $("#datepicker").datepicker( $.datepicker.regional["de"] );

  $.getJSON("index.php?list=location", function(data){
    $("#location").autocomplete({ source: data });
  });
  $.getJSON("index.php?list=city", function(data){
    $("#city").autocomplete({ source: data });
  });
  /*
  $.getJSON("index.php?list=comment", function(data){
    $("#comment").autocomplete({ source: data });
  });
  */
  $.getJSON("index.php?list=group", function(data){
    $("#group").autocomplete({ source: data });
  });

});
