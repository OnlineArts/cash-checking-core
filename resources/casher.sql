-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 13. Okt 2019 um 14:56
-- Server-Version: 10.1.41-MariaDB-0ubuntu0.18.04.1
-- PHP-Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `casher`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cash_city`
--

CREATE TABLE `cash_city` (
  `city_id` int(10) UNSIGNED NOT NULL,
  `city_name` varbinary(300) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Cities';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cash_comment`
--

CREATE TABLE `cash_comment` (
  `comment_id` int(10) UNSIGNED NOT NULL,
  `comment_name` varbinary(500) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Comments';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cash_entry`
--

CREATE TABLE `cash_entry` (
  `entry_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `entry_date` int(10) UNSIGNED NOT NULL,
  `entry_outcome` varbinary(200) DEFAULT NULL,
  `entry_income` varbinary(200) DEFAULT NULL,
  `entry_location` int(10) UNSIGNED DEFAULT NULL,
  `entry_city` int(10) UNSIGNED DEFAULT NULL,
  `entry_comment` int(10) UNSIGNED DEFAULT NULL,
  `entry_group` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Entries';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cash_group`
--

CREATE TABLE `cash_group` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `group_name` varbinary(200) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Groups';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cash_location`
--

CREATE TABLE `cash_location` (
  `location_id` int(10) UNSIGNED NOT NULL,
  `location_name` varbinary(300) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Locations';

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `cash_city`
--
ALTER TABLE `cash_city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indizes für die Tabelle `cash_comment`
--
ALTER TABLE `cash_comment`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indizes für die Tabelle `cash_entry`
--
ALTER TABLE `cash_entry`
  ADD PRIMARY KEY (`entry_id`),
  ADD KEY `entry_location` (`entry_location`),
  ADD KEY `entry_city` (`entry_city`),
  ADD KEY `entry_comment` (`entry_comment`),
  ADD KEY `entry_group` (`entry_group`);

--
-- Indizes für die Tabelle `cash_group`
--
ALTER TABLE `cash_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indizes für die Tabelle `cash_location`
--
ALTER TABLE `cash_location`
  ADD PRIMARY KEY (`location_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `cash_city`
--
ALTER TABLE `cash_city`
  MODIFY `city_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT für Tabelle `cash_comment`
--
ALTER TABLE `cash_comment`
  MODIFY `comment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=677;
--
-- AUTO_INCREMENT für Tabelle `cash_entry`
--
ALTER TABLE `cash_entry`
  MODIFY `entry_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2119;
--
-- AUTO_INCREMENT für Tabelle `cash_group`
--
ALTER TABLE `cash_group`
  MODIFY `group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT für Tabelle `cash_location`
--
ALTER TABLE `cash_location`
  MODIFY `location_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=338;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `cash_entry`
--
ALTER TABLE `cash_entry`
  ADD CONSTRAINT `cash_entry_ibfk_1` FOREIGN KEY (`entry_location`) REFERENCES `cash_location` (`location_id`),
  ADD CONSTRAINT `cash_entry_ibfk_2` FOREIGN KEY (`entry_city`) REFERENCES `cash_city` (`city_id`),
  ADD CONSTRAINT `cash_entry_ibfk_3` FOREIGN KEY (`entry_comment`) REFERENCES `cash_comment` (`comment_id`),
  ADD CONSTRAINT `cash_entry_ibfk_4` FOREIGN KEY (`entry_group`) REFERENCES `cash_group` (`group_id`);

  CREATE VIEW cash_view_entry AS
    SELECT
      en.entry_id id,
      en.entry_date date,
  		en.user_id,
      en.entry_outcome dc_outcome,
      en.entry_income dc_income,
      lo.location_name dc_location,
      ci.city_name dc_city,
      co.comment_name dc_comments,
      gr.group_name dc_groups
    FROM
      `cash_entry` en
      LEFT JOIN
        `cash_location` AS lo
      ON
        en.entry_location = lo.location_id
      LEFT JOIN
        `cash_city` AS ci
      ON
        en.entry_city = ci.city_id
      LEFT JOIN
        `cash_comment` AS co
      ON
        en.entry_comment = co.comment_id

      LEFT JOIN
        `cash_group` AS gr
      ON
        en.entry_group = gr.group_id
    ORDER BY
      en.entry_date DESC,
      en.entry_id DESC

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
